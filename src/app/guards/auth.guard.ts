import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { UserService } from '@app/services/user/user.service';
import { take, map, tap } from 'rxjs/operators';
import { log } from '@app/services/logger.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private userService: UserService) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.userService.getUser().pipe(
      take(1),
      map((user: any) => {
        // console.log(user);
        if (!user || !user.name) {
          return false;
        } else {
          return true;
        }
      }),
      tap(loggedIn => {
        log.info(loggedIn);
        if (!loggedIn) {
          log.warn('access denied');
          this.router.navigate(['/login']);
        }
      })
    );
  }
}
