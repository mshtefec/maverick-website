import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@app/guards/auth.guard';
import { LayoutCADComponent } from '@app/views/cad/layout/layout.component';
import { LayoutVFXComponent } from '@app/views/vfx/layout/layout.component';
import { LandingCADComponent } from '@app/views/cad/landing-cad/landing-cad.component';
import { LandingVFXComponent } from '@app/views/vfx/landing-vfx/landing-vfx.component';
import { FeaturesCADComponent } from '@app/views/cad/features/features.component';
import { LayoutProductComponent } from '@app/views/cad/layout-product/layout-product.component';
import { LoginComponent } from '@app/views/common/login/login.component';
import { DownloadButtonComponent } from '@app/components/download-button/download-button.component';
import { BuyComponent } from '@app/views/common/buy/buy.component';
import { UserSettingsComponent } from '@app/views/common/user-settings/user-settings.component';
import { LandingBrandingComponent } from '@app/views/common/branding/landing-branding.component';
import { DownloadCADComponent } from '@app/views/cad/download/download.component';
import { DownloadVFXComponent } from '@app/views/vfx/download/download.component';
import { LandingComponent } from '@app/views/common/landing/landing.component';
import { RequirementsCADComponent } from '@app/views/cad/requirements/requirements.component';
import { HowItWorksCADComponent } from '@app/views/cad/how-it-works/how-it-works.component';
import { GalleryVFXComponent } from '@app/views/vfx/gallery/gallery-vfx.component';
import { GalleryCADComponent } from '@app/views/cad/gallery/gallery-cad.component';
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  {
    path: 'home',
    component: LandingComponent,
    data: {
      ligthNavbar: true
    }
  },
  { path: 'login', component: LoginComponent },

  {
    path: 'cad',
    component: LayoutCADComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'home' },
      {
        path: 'home',
        component: LandingCADComponent,
        data: {
          ligthNavbar: true
        }
      },
      {
        path: 'product',
        component: LayoutProductComponent,
        data: {
          ligthNavbar: true
        },
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'how-it-works' },
          { path: 'features', component: FeaturesCADComponent },
          { path: 'how-it-works', component: HowItWorksCADComponent },
          { path: 'download', component: BuyComponent },
          { path: 'system-requirements', component: RequirementsCADComponent }
        ]
      },
      { path: 'download', component: DownloadCADComponent },
      { path: 'gallery', component: GalleryCADComponent },
      { path: 'buy', component: BuyComponent },
      { path: 'settings', component: UserSettingsComponent },
      { path: 'gallery', component: GalleryCADComponent }
    ]
  },
  {
    path: 'vfx',
    component: LayoutVFXComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'home' },
      {
        path: 'home',
        component: LandingVFXComponent,
        data: {
          ligthNavbar: true
        }
      },
      { path: 'branding', component: LandingBrandingComponent },
      { path: 'download', component: DownloadVFXComponent },
      { path: 'gallery', component: GalleryVFXComponent },
      { path: 'buy', component: BuyComponent },
      { path: 'settings', component: UserSettingsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
