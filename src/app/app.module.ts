import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from '@app/app-routing.module';
import { AppComponent } from '@app/app.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { environment } from 'environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { MasonryGalleryModule } from 'ngx-masonry-gallery';
import { UserService } from '@app/services/user/user.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from '@app/guards/auth.guard';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { DownloadButtonComponent } from '@app/components/download-button/download-button.component';
import { ModalComponent } from '@app/components/modal/modal.component';
import { FooterComponent } from '@app/components/footer/footer.component';
import { LoaderComponent } from '@app/components/loader/loader.component';
import { ToastContainerComponent } from '@app/components/toast-container/toast-container.component';
import { UserMenuComponent } from '@app/components/user-menu/user-menu.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { RecaptchaModule } from 'ng-recaptcha';
import { VersionManagerService } from '@app/services/version-manager.service';
import { ProductsManagerService } from './services/products.service';
import { ModalStripeComponent } from './components/modal-stripe/modal-stripe.component';
import { LayoutCADComponent } from './views/cad/layout/layout.component';
import { LayoutVFXComponent } from './views/vfx/layout/layout.component';
import { LandingCADComponent } from './views/cad/landing-cad/landing-cad.component';
import { LandingVFXComponent } from './views/vfx/landing-vfx/landing-vfx.component';
import { NavBarCadComponent } from './components/nav-bar-cad/nav-bar-cad.component';
import { FeaturesCADComponent } from './views/cad/features/features.component';
import { GalleryCADComponent } from './views/cad/gallery/gallery-cad.component';
import { LayoutProductComponent } from './views/cad/layout-product/layout-product.component';
import { UserSettingsComponent } from './views/common/user-settings/user-settings.component';
import { LoginComponent } from './views/common/login/login.component';
import { DownloadCADComponent } from './views/cad/download/download.component';
import { DownloadVFXComponent } from './views/vfx/download/download.component';
import { LandingBrandingComponent } from './views/common/branding/landing-branding.component';
import { BuyComponent } from './views/common/buy/buy.component';
import { NavBarVFXComponent } from './components/nav-bar-vfx/nav-bar-vfx.component';
import { LandingComponent } from './views/common/landing/landing.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { RequirementsCADComponent } from './views/cad/requirements/requirements.component';
import { HowItWorksCADComponent } from './views/cad/how-it-works/how-it-works.component';
import { GalleryVFXComponent, ModalContentComponent  } from './views/vfx/gallery/gallery-vfx.component';
import { ModalModule} from 'ngx-bootstrap/modal';
import { GalleryModule } from '@ngx-gallery/core';
import { GALLERY_CONFIG } from '@ngx-gallery/core';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    LayoutCADComponent,
    LayoutVFXComponent,
    LandingCADComponent,
    LandingVFXComponent,
    LoginComponent,
    UserSettingsComponent,
    DownloadCADComponent,
    DownloadVFXComponent,
    DownloadButtonComponent,
    ModalComponent,
    FooterComponent,
    LoaderComponent,
    NavBarComponent,
    NavBarVFXComponent,
    NavBarCadComponent,
    ToastContainerComponent,
    UserMenuComponent,
    LandingBrandingComponent,
    BuyComponent,
    ModalStripeComponent,
    ModalContentComponent,
    FeaturesCADComponent,
    GalleryCADComponent,
    LayoutProductComponent,
    LandingComponent,
    RequirementsCADComponent,
    HowItWorksCADComponent,
    GalleryVFXComponent,
    GalleryCADComponent, 
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    AngularFireDatabaseModule,
    MasonryGalleryModule,
    GalleryModule,
    
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
    NgSelectModule,
    RecaptchaModule,  
    ModalModule.forRoot()
  ],
  entryComponents: [ModalContentComponent],
  providers: [
    UserService, 
    AuthGuard, 
    VersionManagerService, 
    ProductsManagerService,
    {
      provide: GALLERY_CONFIG,
      useValue: {
        dots: true,
        imageSize: 'cover'
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
