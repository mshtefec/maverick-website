import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import * as _ from 'lodash';
import { map, take } from 'rxjs/operators';

@Injectable()
export class ProductsManagerService {
  constructor(private dbfire: AngularFirestore) {}

  getProductList() {
    return this.dbfire.collection(`products`, ref => ref.orderBy('price', 'desc')).valueChanges();
  }
}
