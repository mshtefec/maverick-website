import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';

@Injectable()
export class UserService {
  public user: any;

  constructor(private router: Router, private afAuth: AngularFireAuth, private dbFire: AngularFirestore, private db: AngularFireDatabase) {
    this.user = this.afAuth.authState.pipe(
      switchMap(userAuth => {
        if (userAuth) {
          const userRef = this.db.database.ref(`onlineUsers/${userAuth.uid}`);
          userRef.set(this.getBrowser());
          userRef.onDisconnect().remove();

          return this.dbFire
            .doc(`users/${userAuth.uid}`)
            .snapshotChanges()
            .pipe(
              map(snap => {
                if (snap.payload.exists) {
                  const userDb = snap.payload.data();
                  return userDb;
                } else {
                  console.error('User not found');
                  return null;
                }
              })
            );
        } else {
          // this.playerService.setPlayerAsProfile('anonymous');
          console.error('User not found');
          return of(null);
        }
      })
    );
  }

  getAuthState() {
    return this.afAuth.authState;
  }

  getAuth() {
    return this.afAuth.auth;
  }

  getUser() {
    return this.user;
  }

  getUserPromise() {
    return this.user.pipe(take(1)).toPromise();
  }

  logout(notRedirect?) {
    this.afAuth.auth.signOut().then(() => {
      if (!notRedirect) {
        this.router.navigate(['/']);
      }
    });
  }

  newUser(userId, userData, status) {
    const user = {
      key: userId,
      status: status,
      signUpDate: new Date(),
      versionType: 'release',
      roles: { user: true },
      ...userData
    };

    return this.dbFire.doc(`users/${userId}`).set(user);
  }

  userUpdate(userId, userData) {
    return this.dbFire.doc(`users/${userId}`).update({
      status: 'ok',
      ...userData
    });
  }

  emailSubscribe(email) {
    return new Promise((resolve, reject) => {
      this.dbFire
        .collection(`emailList`, ref => ref.where('email', '==', email))
        .valueChanges()
        .pipe(take(1))
        .toPromise()
        .then(data => {
          console.log(data);

          if (!data.length) {
            this.dbFire
              .collection(`emailList`)
              .add({
                email,
                timestamp: new Date()
              })
              .then(() => resolve(true))
              .catch(err => reject(err));
          } else {
            reject(false);
          }
        });
    });
  }

  getBrowser() {
    let sBrowser;
    const sUsrAg = navigator.userAgent;

    if (sUsrAg.indexOf('Chrome') > -1) {
      sBrowser = 'Chrome';
    } else if (sUsrAg.indexOf('Safari') > -1) {
      sBrowser = 'Safari';
    } else if (sUsrAg.indexOf('Opera') > -1) {
      sBrowser = 'Opera';
    } else if (sUsrAg.indexOf('Firefox') > -1) {
      sBrowser = 'Firefox';
    } else if (sUsrAg.indexOf('MSIE') > -1) {
      sBrowser = 'Explorer';
    }
    return sBrowser;
  }
}
