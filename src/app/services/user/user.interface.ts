export interface UserInterface {
  email: string;
  img: string;
  role?: 'admin' | 'prime' | 'basic' | 'anonymous';
  status: string;
  country: string;
  name: string;
  key: string;
  beta: boolean;
  versionType: string;
  prime?: string;
  signUpDate?: string;
}
