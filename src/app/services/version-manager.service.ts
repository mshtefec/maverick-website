import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import * as _ from 'lodash';
import { map, take } from 'rxjs/operators';

@Injectable()
export class VersionManagerService {
  constructor(private dbfire: AngularFirestore) {}

  getReleases() {
    return this.dbfire.collection(`Releases`, ref => ref.orderBy('versionNumber', 'desc')).valueChanges();
  }

  getDownloadLink(versionType): Promise<string> {
    return new Promise((resolve, reject) => {
      if (!versionType || versionType === 'undefined') {
        versionType = 'release';
      }
      const versionMap = {
        dev: 4,
        alpha: 3,
        beta: 2,
        release: 1
      };

      this.dbfire
        .collection(`Releases`, ref =>
          ref
            .orderBy('type', 'desc')
            .where('type', '<=', versionMap[versionType])
            .orderBy('versionNumber', 'desc')
        )
        .snapshotChanges()
        .pipe(
          map(actions =>
            actions.map(a => {
              const data = a.payload.doc.data();
              const key = a.payload.doc.id;
              return { key, ...data };
            })
          )
        )
        .pipe(take(1))
        .toPromise()
        .then((data: any) => {
          // console.log(_.orderBy(data, ['versionNumber'], 'desc'));
          resolve(_.orderBy(data, ['versionNumber'], 'desc')[0]);
        })
        .catch(err => reject(err));
    });
  }

  updateDownloadNum(userId, versionKey) {
    const ref = this.dbfire.doc(`Releases/${versionKey}`);
    ref
      .valueChanges()
      .pipe(take(1))
      .toPromise()
      .then((version: any) => ref.update({ downloadNum: version.downloadNum + 1 }));
    if (userId) {
      ref.collection('downloads').add({
        timestamp: Date.now(),
        userId
      });
    }
  }
}
