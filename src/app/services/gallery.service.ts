import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

    private basePath = '/gallery';
    list: AngularFirestoreCollection<any>;

    constructor(
      private firestore: AngularFirestore
    ) { }

    getImages() {
      return this.firestore.collection('gallery').snapshotChanges();
    }
    getImagesByCategory(category: string) {
      return this.firestore.collection('gallery', ref => ref.where('category', '==', category).orderBy('order')).snapshotChanges();  
    }
}