import { environment } from 'environments/environment';

export const log = {
  trace: (...data) => {
    if (environment.logLevel >= 5) {
      console.log(...data);
    }
  },
  debug: (...data) => {
    if (environment.logLevel >= 4) {
      console.log(...data);
    }
  },
  info: (...data) => {
    if (environment.logLevel >= 3) {
      console.log(...data);
    }
  },
  warn: (...data) => {
    if (environment.logLevel >= 2) {
      console.warn(...data);
    }
  },
  error: (...data) => {
    if (environment.logLevel >= 1) {
      console.error(...data);
    }
  },
  fatal: (...data) => {
    if (environment.logLevel >= 0) {
      console.error(...data);
    }
  }
};
