import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface ToastInterface {
  content: string;
  sub?: string;
  style: 'info' | 'error' | 'empty' | 'min' | 'custom' | 'loading';
  img?: string;
  close: boolean;
  time?: number;
  link?: string;
  html?: string;
}

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  private _toastManager = new BehaviorSubject(null);

  constructor() {}

  get() {
    return this._toastManager.asObservable();
  }

  add(toast: ToastInterface) {
    this._toastManager.next(toast);
  }
}
