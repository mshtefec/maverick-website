export interface Releases {
  date: Date;
  downloadNum: number;
  type: number;
  updateNum: number;
  url: string;
  versionNumber: string;
  versionString: string;
}
