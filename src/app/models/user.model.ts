export interface User {
  key: string;

  nickname: string;
  country: string;
  img: string;
  platform: string;
  signUpDate: Date;
  status: string;
  versionType?: string;

  lastvisit: Date;
  fullname: string;
  company: string;
  email: string;
  resellerid: number;
  address: string;
  url: string;
  notes: string;
}
