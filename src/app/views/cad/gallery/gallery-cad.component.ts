import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastService } from '@app/services/toast.service';
import { UserService } from '@app/services/user/user.service';
import { GalleryService } from '@app/services/gallery.service';
import { IMasonryGalleryImage } from 'ngx-masonry-gallery';
declare const drift;

@Component({
  selector: 'app-gallery-cad-cad',
  templateUrl: './gallery-cad.component.html',
  styleUrls: ['./gallery-cad.component.scss']
})
export class GalleryCADComponent implements OnInit {
  public _videoBg = '';
  public _downloadLink;
  @ViewChild('videoBg')
  videoBg: ElementRef;
  //public list = [];
  private urls: string[] = [];

  constructor(private userService: UserService, private toastService: ToastService, private galleryService: GalleryService) {}

  ngOnInit() {
    this.galleryService.getImagesByCategory('cad').subscribe((fileSnapshot) => {
      //this.list = [];
      fileSnapshot.forEach((fileData: any) => {
        // this.list.push({
        //   id: fileData.payload.doc.id,
        //   data: fileData.payload.doc.data()
        // });
        this.urls.push(fileData.payload.doc.data().url);
      })
    });
  }

  public get images(): IMasonryGalleryImage[] {
    return this.urls.map(m => <IMasonryGalleryImage>{
      imageUrl: m
    });
  }
}
