import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastService } from '@app/services/toast.service';
import { UserService } from '@app/services/user/user.service';
declare const drift;

@Component({
  selector: 'app-features-cad',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss']
})
export class FeaturesCADComponent implements OnInit {
  public _videoBg = '';
  public _downloadLink;
  @ViewChild('videoBg')
  videoBg: ElementRef;

  constructor(private userService: UserService, private toastService: ToastService) {}

  ngOnInit() {}
}
