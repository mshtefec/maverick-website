import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { environment } from 'environments/environment';
declare const drift;

@Component({
  selector: 'app-layout-vfx',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutVFXComponent implements OnInit, OnDestroy {
  public _hiddenNavbar = false;
  public _ligthNavbar = false;
  public _hiddenFooter = false;
  public _hiddenDiscord = false;
  public _hiddenBg = false;
  public _minimalFooter = false;
  public user;
  private routeSub;
  public _cookies = false;

  constructor(private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {
    this.setDataParams();
    this.routeSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.setDataParams();
        window.scrollTo(0, 0);
      }
    });

    const cookies = localStorage.getItem('Maverick-Cookies');
    if (!cookies) {
      this._cookies = true;
    }
  }

  ngOnDestroy() {
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }

  setDataParams() {
    if (this.route.snapshot.firstChild) {
      const routeData = this.route.snapshot.firstChild.data;
      this._hiddenNavbar = routeData['hiddenNavbar'];
      this._ligthNavbar = routeData['ligthNavbar'];
      this._hiddenDiscord = routeData['hiddenDiscord'];
      this._hiddenFooter = routeData['hiddenFooter'];
      this._hiddenBg = routeData['hiddenBg'];
      this._minimalFooter = routeData['minimalFooter'];
    }
  }

  closeCookies() {
    localStorage.setItem('Maverick-Cookies', 'true');
    this._cookies = false;
  }
}
