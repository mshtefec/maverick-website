import { Component, OnInit } from '@angular/core';
import { UserService } from '@app/services/user/user.service';
import { IMasonryGalleryImage } from 'ngx-masonry-gallery';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { Gallery, GalleryRef, ImageItem } from '@ngx-gallery/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

declare const drift;

@Component({
  selector: 'app-gallery-vfx-cad',
  templateUrl: './gallery-vfx.component.html',
  styleUrls: ['./gallery-vfx.component.scss']
})
export class GalleryVFXComponent implements OnInit {
  
  private urls: string[] = [];
  bsModalRef: BsModalRef; 

  constructor(
    private userService: UserService,
    private angularFirestore: AngularFirestore,
    private storage: AngularFireStorage,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.angularFirestore.collection('gallery').get().toPromise().then(
      (snapshot) => {
        snapshot.docs.forEach(doc => {
        this.storage.ref('gallery/' + doc.data().name).getDownloadURL().subscribe((fileSnapshot) => {
            this.urls.push(fileSnapshot);
          })
        })
      }
    )
    
  }

  public get images(): IMasonryGalleryImage[] {
    return this.urls.map(m => <IMasonryGalleryImage>{
      imageUrl: m,
    });
  }

  displayCounter(image) {
    console.log(image);
    this.openModalWithComponent(image.imageUrl);
  }

  openModalWithComponent(url: string) {
    const initialState = {
      image: url,
      title: 'File Show'
    };
    this.bsModalRef = this.modalService.show(ModalContentComponent, {class: 'modal-lg'});
    this.bsModalRef.content.closeBtnName = 'Close';
  }
}

@Component({
  selector: 'modal-content',
  template: `
    <gallery id="myGallery"></gallery>
  `
})
export class ModalContentComponent implements OnInit {
  title: string;
  closeBtnName: string;
  image: string;

  private urls: string[] = [];
 
  constructor(
    public bsModalRef: BsModalRef,
    private gallery: Gallery,
    private angularFirestore: AngularFirestore,
    private storage: AngularFireStorage
  ) {

  }
 
  ngOnInit() {
    const galleryRef = this.gallery.ref('myGallery');
    this.angularFirestore.collection('gallery').get().toPromise().then(
      (snapshot) => {
        snapshot.docs.forEach(doc => {
        this.storage.ref('gallery/' + doc.data().name).getDownloadURL().subscribe((fileSnapshot) => {
            this.urls.push(fileSnapshot);
            galleryRef.addImage({ src: fileSnapshot, thumb: 'IMAGE_THUMBNAIL_URL', title: doc.data().author });
          })
        })
      }
    )   
  }
  
}