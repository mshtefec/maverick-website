import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import * as firebase from 'firebase';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '@app/services/user/user.service';
import { LanguagesModel } from '@app/models/languages.model';
import { log } from '@app/services/logger.service';
import { ToastService } from '@app/services/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  public user;
  public userLoaded = false;
  public avatarList = ['avatar1', 'avatar2', 'avatar3', 'avatar4'];
  public languageModel = LanguagesModel;
  public userModel = {
    platform: '',
    img: '',
    name: '',
    email: '',
    country: null
  };
  public userToLogin = {
    email: '',
    password: ''
  };
  public loginErrors;
  private userSub;
  public loginLoading = false;
  public _validCode = false;
  public _policies = false;
  public _captcha = true;

  public _forgotEmail = '';

  @ViewChild('forgotPasswordModal')
  forgotPasswordModal;

  constructor(private router: Router, private userService: UserService, private toastService: ToastService) {}

  ngOnInit() {
    this.checkUser();

    // Random initial avatar
    const randomAvatar = Math.floor(Math.random() * 2);
    this.userModel.img = this.avatarList[randomAvatar];
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
    if (!this.user || !this.user.name) {
      this.clearLogin();
    }
  }

  checkUser() {
    // User subscription
    this.userSub = this.userService.getUser().subscribe(user => {
      this.userLoaded = true;
      log.info('User: ', user);
      if (user) {
        this.user = user;
        console.log(user);
        this.router.navigateByUrl('/rooms');
      }
    });
  }

  login(loginPlatform, userData = null) {
    this.loginLoading = true;
    switch (loginPlatform) {
      case 'password':
        this.userService
          .getAuth()
          .signInWithEmailAndPassword(userData.email, userData.password)
          .catch(err => {
            this.loginErrors = err.message;
          });
        break;
      case 'google':
        this.userService
          .getAuth()
          .signInWithPopup(new firebase.auth.GoogleAuthProvider())
          .then(data => {
            this.newMinUser(data.user, loginPlatform);
          });
        break;
      case 'twitter':
        this.userService
          .getAuth()
          .signInWithPopup(new firebase.auth.TwitterAuthProvider())
          .then(data => {
            this.newMinUser(data.user, loginPlatform);
          });
        break;
      case 'facebook':
        this.userService
          .getAuth()
          .signInWithPopup(new firebase.auth.FacebookAuthProvider())
          .then(data => {
            this.newMinUser(data.user, loginPlatform);
          });
        break;
    }
  }

  newMinUser(user, platform) {
    this.userService.getUserPromise().then(userProm => {
      if (!userProm) {
        this.userService
          .newUser(user.uid, { email: user.email }, 'ok')
          .then(() => {
            this.selectLogin(platform);
            this.loginLoading = false;
            console.log('OKKOKOKOK');
          })
          .catch(err => {
            this.loginLoading = false;
            console.error(err);
          });
      } else {
        this.selectLogin(platform);
      }
    });
  }

  createUser(loginPlatform) {
    if (loginPlatform === 'password') {
      this.userService
        .getAuth()
        .createUserWithEmailAndPassword(this.userModel.email, this.userToLogin.password)
        .then(userData => {
          console.log('User created on firebase', userData);
          this.userService.newUser(userData.user.uid, this.userModel, 'ok').then(() => {
            // this.useReferralCode(userData.user.uid, this.userModel.name);
            this.loginRedirect();
          });
        })
        .catch(err => {
          this.loginErrors = err;
        });
    } else {
      console.log(this.user);

      this.userService
        .userUpdate(this.user.key, {
          img: this.userModel.img,
          country: this.userModel.country,
          name: this.userModel.name,
          platform: this.userModel.platform
        })
        .then(data => {
          // this.useReferralCode(this.user.key, this.userModel.name);
          this.loginRedirect();
        });
    }
  }

  selectLogin(loginPlatform) {
    this.userModel.platform = loginPlatform;
  }

  loginRedirect() {
    this.router.navigateByUrl('/cad');
  }

  clearLogin() {
    this.userService
      .getAuth()
      .signOut()
      .then(() => {
        this.userModel.platform = null;
        this.user = null;
      });
  }

  resolved(event) {
    log.info(`Captcha event: ${event}`);
    this._captcha = event ? true : false;
  }

  forgotPassword(email) {
    this.userService.getAuth().sendPasswordResetEmail(email);
    this.forgotPasswordModal.close();
    this.toastService.add({
      content: '@@Login@@CheckYourMail',
      sub: email.value,
      style: 'loading',
      close: true,
      time: 5000
    });
    this._forgotEmail = '';
  }
}
