import { Component, OnInit } from '@angular/core';
import { UserService } from '@app/services/user/user.service';
import { LanguagesModel } from '@app/models/languages.model';

@Component({
  selector: 'dixper-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss']
})
export class UserSettingsComponent implements OnInit {
  public avatarList = ['avatar1', 'avatar2', 'avatar3', 'avatar4'];
  public tab = 'profile';
  public languageModel = LanguagesModel;
  public _userId;
  public lenguageId;
  public urlPage;
  public _userModel = {
    nickname: '',
    fullname: '',
    company: '',
    img: '',
    country: '',
    versionType: 'release',
    notes: ''
  };

  public _beta = false;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.userService.getUser().subscribe(user => {
      this._userId = user.key;
      this._userModel = {
        nickname: user.nickname || 'Nickname',
        fullname: user.fullname || 'Fullname',
        company: user.company || 'N/A',
        img: user.img || 'avatar1',
        country: user.country || 'N/A',
        versionType: user.versionType || 'release',
        notes: user.notes || 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex...'
      };
      this.lenguageId = user.country || 'N/A',
      this.urlPage = user.url || 'dont have a license...',
      this._beta = user.versionType === 'beta' || user.versionType === 'alpha' || user.versionType === 'dev' ? true : false;
    });
  }

  saveProfile() {
    this.userService.userUpdate(this._userId, this._userModel);
  }

  updateBetaUser(event) {
    let versionType = 'release';
    if (event.target.checked) {
      versionType = 'beta';
    }
    this.userService.userUpdate(this._userId, { versionType });
  }
}
