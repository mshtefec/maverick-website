import { Component, OnInit, ElementRef, AfterViewInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductsManagerService } from '@app/services/products.service';
declare var Stripe: any;

const stripe = Stripe('pk_test_9tHETYwaRBF42KrFSPLFsROZ');
const elements = stripe.elements();
const card = elements.create('card');

@Component({
  selector: 'dixper-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.scss']
})
export class BuyComponent implements OnInit {
  public _productList: Observable<any>;

  constructor(private productManager: ProductsManagerService) {}

  ngOnInit() {
    this._productList = this.productManager.getProductList();
  }
}
