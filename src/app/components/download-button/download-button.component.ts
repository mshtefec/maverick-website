import { Component, OnInit } from '@angular/core';
import { VersionManagerService } from '@app/services/version-manager.service';
import { UserService } from '@app/services/user/user.service';
import { ToastService } from '@app/services/toast.service';

@Component({
  selector: 'maverick-download-button',
  templateUrl: './download-button.component.html',
  styleUrls: ['./download-button.component.scss']
})
export class DownloadButtonComponent implements OnInit {
  public _user;
  public _downloadLink;

  constructor(private userService: UserService, private versionManager: VersionManagerService, private toastService: ToastService) {}

  ngOnInit() {
    this.userService.getUserPromise().then(user => {
      this._user = user;
      let versionType = 'release';

      if (user && user.versionType) {
        versionType = user.versionType;
      }

      this.versionManager.getDownloadLink(versionType).then(downloadLink => {
        this._downloadLink = downloadLink;
      });
    });
  }
  downloadServer() {
    this.toastService.add({
      content: '@@RoomList@@Downloading',
      sub: '@@RoomList@@DownloadingSub',
      style: 'loading',
      close: true,
      time: 5000
    });
    if (this._user) {
      this.versionManager.updateDownloadNum(this._user.key, this._downloadLink.key);
    } else {
      this.versionManager.updateDownloadNum(null, this._downloadLink.key);
    }
  }
}
