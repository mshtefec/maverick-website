import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { UserService } from '@app/services/user/user.service';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'dixper-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  animations: [
    trigger('userMenuAnimation', [
      state('close', style({ opacity: '0', transform: 'translateY(-20px)', display: 'none' })),
      state('open', style({ opacity: '1', transform: 'translateY(-10px)' })),
      transition('close <=> open', animate('150ms ease'))
    ]),
    trigger('userBgAnimation', [
      state('close', style({ opacity: '0', display: 'none' })),
      state('open', style({ opacity: '1' })),
      transition('close <=> open', animate('250ms ease-in'))
    ])
  ]
})
export class UserMenuComponent implements OnInit, OnDestroy {
  public user;
  public userLoaded = false;
  @Input()
  public darkNavbar;
  public userMenuActive = 'close';
  public _languageMenu = false;
  public _languages;
  public _languageSelected;
  private userSub;
  public _mobileMenu = false;
  public _languageMap = { es: 'Spanish', en: 'English' };

  constructor(private translateService: TranslateService, private router: Router, private userService: UserService) {}

  ngOnInit() {
    this.userSub = this.userService.getUser().subscribe(user => {
      this.userLoaded = true;
      this.user = user;
    });
  }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }

  login() {
    this.userService.getAuth().signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }

  logout() {
    this.toggleMenu();
    this.userService.logout();
  }

  toggleMenu() {
    this.userMenuActive = this.userMenuActive === 'open' ? 'close' : 'open';
  }
}
