import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';

@Component({
  selector: 'dixper-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  animations: [
    trigger('modalMenuAnimation', [
      state('close', style({ opacity: '0', transform: 'scale(.75)' })),
      state('open', style({ opacity: '1', transform: 'scale(1)' })),
      transition('close <=> open', animate('250ms cubic-bezier(.175,.885,.32,1.275)'))
    ]),
    trigger('modalBgAnimation', [
      state('close', style({ opacity: '0', display: 'none' })),
      state('open', style({ opacity: '1' })),
      transition('close <=> open', animate('250ms ease-in-out'))
    ])
  ]
})
export class ModalComponent {
  public _state = 'close';

  @Input() padding = 30;
  @Input() height = 320;
  @Input() width = 600;
  @Input() bg = '#fff';

  @Output() closeEvent = new EventEmitter();
  @Output() openEvent = new EventEmitter();

  constructor() {}

  public open() {
    this._state = 'open';
    this.openEvent.emit(true);
  }

  public close() {
    this._state = 'close';
    this.closeEvent.emit(true);
  }

  public toggle() {
    this._state = this._state === 'open' ? 'close' : 'open';
  }
}
