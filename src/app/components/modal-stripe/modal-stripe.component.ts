import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes } from '@angular/animations';
declare var Stripe: any;

const stripe = Stripe('pk_test_9tHETYwaRBF42KrFSPLFsROZ');
const elements = stripe.elements();
const card = elements.create('card');

@Component({
  selector: 'dixper-modal-stripe',
  templateUrl: './modal-stripe.component.html',
  styleUrls: ['./modal-stripe.component.scss'],
  animations: [
    trigger('modalAnimation', [
      state('close', style({ opacity: '0', display: 'none' })),
      state('open', style({ opacity: '1' })),
      transition('close <=> open', animate('250ms cubic-bezier(.175,.885,.32,1.275)'))
    ]),
    trigger('modalMenuAnimation', [
      state('close', style({ opacity: '0', transform: 'scale(.75)' })),
      state('open', style({ opacity: '1', transform: 'scale(1)' })),
      transition('close <=> open', animate('250ms cubic-bezier(.175,.885,.32,1.275)'))
    ]),
    trigger('modalBgAnimation', [
      state('close', style({ opacity: '0', display: 'none' })),
      state('open', style({ opacity: '1' })),
      transition('close <=> open', animate('250ms ease-in-out'))
    ])
  ]
})
export class ModalStripeComponent implements AfterViewInit {
  public _state = 'close';

  @Output()
  closeEvent = new EventEmitter();
  @Output()
  openEvent = new EventEmitter();

  @ViewChild('cardForm')
  cardForm: ElementRef;

  constructor() {}

  ngAfterViewInit() {
    card.mount(this.cardForm.nativeElement);
  }

  async handleForm(e) {}

  public open() {
    this._state = 'open';
    this.openEvent.emit(true);
  }

  public close() {
    this._state = 'close';
    this.closeEvent.emit(true);
  }

  public toggle() {
    this._state = this._state === 'open' ? 'close' : 'open';
  }
}
