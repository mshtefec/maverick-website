import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'dixper-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  @Input()
  minimal;
  public _version = environment.version;

  public _languageMenu = false;
  public _languages;
  public _languageSelected;
  public _languageMap = { es: 'Spanish', en: 'English' };
  public userMenuActive = 'close';

  constructor(private translateService: TranslateService) {}

  ngOnInit() {
    this._languageSelected = this.translateService.currentLang;
    this._languages = this.translateService.getLangs();
  }

  toggleMenu() {
    this.userMenuActive = this.userMenuActive === 'open' ? 'close' : 'open';
  }

  selectLang(language) {
    this.translateService.use(language);
    this._languageSelected = language;
    localStorage.setItem('Dixper-Lang', language);

    this._languageMenu = false;
  }
}
