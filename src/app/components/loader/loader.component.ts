import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'dixper-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {
  @Input()
  public item = '';
  @Input()
  public height;
  @Input()
  public message = '';

  constructor() {}

  ngOnInit() {}
}
