import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastService, ToastInterface } from '@app/services/toast.service';
import { Router } from '@angular/router';

@Component({
  selector: 'dixper-toast-container',
  templateUrl: './toast-container.component.html',
  styleUrls: ['./toast-container.component.scss']
})
export class ToastContainerComponent implements OnInit {
  public _toastList: Array<ToastInterface> = [];
  @ViewChild('AudioManager')
  public _AudioManager;

  constructor(private router: Router, private toastService: ToastService) {}

  ngOnInit() {
    this.toastService.get().subscribe((toast: ToastInterface) => {
      if (toast) {
        this.pushToast(toast);
      }
    });
  }

  pushToast(toast: ToastInterface) {
    if (toast.time) {
      setTimeout(() => this.removeToast(toast), toast.time);
    }
    this._toastList.push(toast);
    this.playNotificationSound();
  }

  removeToast(toast: ToastInterface) {
    const index = this._toastList.indexOf(toast);
    this._toastList.splice(index, 1);
  }

  playNotificationSound() {
    const audio: HTMLAudioElement = this._AudioManager.nativeElement;
    audio.volume = 0.2;
    audio.pause();
    audio.currentTime = 0;
    audio.play();
  }

  goTo(link: string) {
    this.router.navigateByUrl(link);
  }

  openLink(link) {
    if (link) {
      window.open(link, '_blank');
    }
  }
}
